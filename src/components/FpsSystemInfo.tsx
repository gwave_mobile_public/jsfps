import dayjs from 'dayjs';

const FpsSystemInfo = ({ symtemData }) => {
  return (
    <ul>
      <li>时间：{dayjs(symtemData.time).format('YYYY-MM-DD HH:mm:ss')}</li>
      <li>平台：{symtemData.os}</li>
      <li>手机品牌：{symtemData.deviceBrand}</li>
      <li>手机版本：{symtemData.deviceVersion}</li>
      <li>App版本：{symtemData.appVersion}</li>
    </ul>
  );
};

export default FpsSystemInfo;
