import { useLayoutEffect, useRef } from 'react';
import { jsx, Canvas, Chart, Line, Axis, Tooltip } from '@antv/f2';
const FpsChart = ({ name, fpsdata }) => {
  const chartRef = useRef(null);
  useLayoutEffect(() => {
    let newData = fpsdata;
    if (fpsdata.length === 1) {
      newData = fpsdata.concat(fpsdata);
    }
    const data = newData.map((val, index) => {
      return { time: index, fps: val };
    });
    console.log(data);
    const scale = {
      time: {},
      fps: {
        tickCount: 5,
        min: 0,
        max: 60,
        alias: 'JSFPS'
      }
    };
    const context = chartRef.current.getContext('2d');
    const LineChart = (
      <Canvas
        width={1000}
        context={context}
        pixelRatio={window.devicePixelRatio}
      >
        <Chart data={data} scale={scale}>
          <Axis
            field="time"
            style={{
              label: null
            }}
          />
          <Axis field="fps" />
          <Line x="time" y="fps" shape="smooth" />
          <Tooltip />
        </Chart>
      </Canvas>
    );

    const chart = new Canvas(LineChart.props);
    chart.render();
  }, []);
  return (
    <>
      <h3>{name}</h3>
      <canvas id="container" ref={chartRef} />
    </>
  );
};

export default FpsChart;
